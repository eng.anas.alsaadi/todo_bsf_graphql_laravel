<?php

namespace App\GraphQL\Queries;

use App\Models\Task;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;


class TaskQuery extends Query
{

    protected $attributes = [
        'name' => 'task',
    ];

    public function type():Type
    {
        return GraphQL::type('Task');
    }

    public function args():array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required']
            ],
        ];
    }
    public function resolve($root, $args)
    {
        return Task::find($args['id']);
    }

}
