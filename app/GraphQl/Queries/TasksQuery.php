<?php

namespace App\GraphQL\Queries;

use App\Models\Task;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;

use Closure;

class TasksQuery extends Query
{

    protected $attributes = [
        'name' => 'tasks',
    ];

    public function type(): Type
    {
//        return Type::listOf(GraphQL::type('Task'));
        return GraphQL::paginate('Task');

    }

    public function args(): array
    {
        return [
            'limit' => [
                'name' => 'limit',
                'type' => Type::int(),
                'rules' => ['required']
            ],
            'page' => [
                'name' => 'page',
                'type' => Type::int(),
                'rules' => ['required']
            ],
            'status' => [
                'name' => 'status',
                'type' => Type::int(),
            ],
        ];
    }


    public function resolve($root, array $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();

        $status_arr = $args['status'] == 0 ? [1, 2, 3, 4] : [$args['status']];

        return Task::with($fields->getRelations())->whereIn('status', $status_arr)
            ->orderBy('date', 'asc')
            ->select($fields->getSelect())
            ->paginate($args['limit'], ['*'], 'page', $args['page']);
    }

}
