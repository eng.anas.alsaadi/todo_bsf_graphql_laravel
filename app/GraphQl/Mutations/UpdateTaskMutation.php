<?php

namespace App\GraphQL\Mutations;

use App\Models\Task;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;


class UpdateTaskMutation extends Mutation
{
    protected $attributes = [
        'name' => 'updateTask',
        'description' => 'Update a Task'
    ];

    public function type(): Type
    {
        return GraphQL::type('Task');
    }

    public function args(): array
    {
        return [

            'date' => [
                'name' => 'date',
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'id' => [
                'name' => 'id',
                'type' => Type::ID(),
                'rules' => ['required']
            ],
            'status' => [
                'name' => 'status',
                'type' => Type::INT(),
                'rules' => ['required']
            ],
        ];
    }

    public function validationErrorMessages(array $args = []): array
    {
        return [
            'id.required' => 'Please enter ID of task',
            'title.required' => 'Please enter title of task',
            'description.required' => 'Please enter description of task',
            'date.required' => 'Please enter description of task',
        ];
    }

    public function resolve($root, $args)
    {

        $task = Task::findOrFail($args['id']);
        $args['date'] = date('Y-m-d H:i', strtotime($args['date']));

        $task->fill($args);
        $task->save();

        return $task;
    }

}
