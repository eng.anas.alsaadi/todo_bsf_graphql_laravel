<?php

namespace App\GraphQL\Mutations;

use App\Models\Task;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;


class CreateTaskMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createTask',
        'description' => 'Creates a Task'
    ];

    public function type(): Type
    {
        return GraphQL::type('Task');
    }

    public function args(): array
    {
        return [
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
                'message' => "not null",
                'rules' => ['required']
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string(),
                'message' => "not null",
                'rules' => ['required']
            ],
            'date' => [
                'name' => 'date',
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'status' => [
                'name' => 'status',
                'type' => Type::int(),
                'rules' => ['required']
            ],
        ];
    }

    public function validationErrorMessages(array $args = []): array
    {
        return [
            'title.required' => 'Please enter title of task',
            'description.required' => 'Please enter description of task',
        ];
    }

    public function resolve($root, $args)
    {
        $task = new Task();
        $args['date'] = date('Y-m-d H:i', strtotime($args['date']));
        $task->fill($args);
        $task->save();

        return $task;
    }

}
