<?php

namespace App\GraphQL\Mutations;

use App\Models\Task;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;


class RemoveTaskMutation extends Mutation
{
    protected $attributes = [
        'name' => 'removeTask',
        'description' => 'Remove a Task'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [

            'id' => [
                'name' => 'id',
                'type' => Type::ID(),
                'rules' => ['required']
            ],
        ];
    }

    public function validationErrorMessages(array $args = []): array
    {
        return [
            'id.required' => 'Please enter ID of task',

        ];
    }

    public function resolve($root, $args)
    {

        $task = Task::findOrFail($args['id']);

        return $task->delete() ? true : false;
    }

}
