<?php

namespace App\GraphQL\Types;

use App\Models\Task;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;


class TaskType extends GraphQLType
{

    protected $attributes = [
        'name' => 'Task',
        'description' => 'Details of Task',
        'model' => Task::class
    ];


    public function fields(): array
    {
        return [
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'title of the task',
            ], 'description' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'description of the task',
            ],
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Id of the task',
            ],
            'status' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'Status of the task',
            ],
            'date' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Date of the task',
            ],
        ];
    }


}
