<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // php artisan db:seed
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 25; $i++) {

            $t = new Task([

                "title" => $faker->title,
                "description" => $faker->text,
                "status" => rand(1, 4),
                "date" => date('Y-m-d H:i'),
            ]);
            $t->save();
        }
    }
}
